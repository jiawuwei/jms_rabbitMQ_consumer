package cn.jia.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by jia on 2018/4/16.
 */
@Controller
@RequestMapping(value = "/")
public class TestController {
    @RequestMapping(value = {"","test"})
    public String test(){
        return "index";
    }
}
