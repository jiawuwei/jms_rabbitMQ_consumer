package cn.jia.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

/**
 * Created by jia on 2018/4/4.
 */
/*
@Configuration
//启用SpringMVC
@EnableWebMvc
//只扫描web下的应用
@ComponentScan("cn.jia.web")
public class WebConfig extends WebMvcConfigurerAdapter{
    //配置视图解析器
    @Bean
    public ViewResolver viewResolver(){
        InternalResourceViewResolver resolver = new InternalResourceViewResolver();
        resolver.setPrefix("/WEB-INF/views/");
        resolver.setPrefix(".jsp");
        resolver.setExposeContextBeansAsAttributes(true);
        return resolver;
    }

    */
/**
     * 配置静态资源处理
     * @param configurer
     *//*

    @Override
    public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer) {
        //通过调用DefaultServlet-
        //HandlerConfigurer的enable()方法，我们要求DispatcherServlet将对静态资源
        //的请求转发到Servlet容器中默认的Servlet上，而不是使用DispatcherServlet本身来处理
        //此类请求。
        configurer.enable();
    }
}
*/
