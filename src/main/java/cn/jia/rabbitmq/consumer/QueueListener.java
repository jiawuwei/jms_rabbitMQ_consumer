package cn.jia.rabbitmq.consumer;

import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageListener;

import java.io.UnsupportedEncodingException;

/**
 * @Author: jia
 * @Descirption:
 * @Date: 2018/4/20 12:05
 * @Modified By:
 */


public class QueueListener implements MessageListener {
    @Override
    public void onMessage(Message message) {
        try {
            System.out.println(new String(message.getBody(),"utf-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

    }
}
